#include "read_write.h"
#include "rotation.h"

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("!!!Need two arguments!!!");
        return -1;
    }
    char *pathin = argv[1];
    char *pathout = argv[2];
    FILE *in= fopen(pathin, "rb");
    if (!in)return -1;
    struct image source;
    if (from_bmp(in, &source)) {
        fclose(in);
        free(source.data);
        return -1;
    }
    FILE *out = fopen(pathout, "wb");
    if (!out) {
        fclose(in);
        return -1;
    }
    struct image rot_img = rotate(source);
    free(source.data);
    if (to_bmp(out, &rot_img)) {
        free(rot_img.data);
        fclose(in);
        fclose(out);
        return -1;
    }
    fclose(in);
    fclose(out);
    free(rot_img.data);
    return 0;
}
