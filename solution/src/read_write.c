#include "read_write.h"
#include <stdbool.h>
#include  <stdint.h>
#define BFTYPE 0x4d42
#define BFRESERVED 0
#define B0FFBITS 54
#define BISIZE 40
#define BIPLANES 1
#define BIBITCOUNT 24
#define BICOMPRESSION 0
#define BIX 2834
#define BIY 2834
#define BICLRSUSED 0
#define BICLRIMPORTANT 0
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

struct bmp_header generate_bmp_header(struct image const *img) {
    return (struct bmp_header) {
            BFTYPE,
            img->width * img->height * 3 + B0FFBITS + img->height * (4 - (img->width * 3) % 4) % 4 + 54,
            BFRESERVED,
            B0FFBITS,
            BISIZE,
            img->width,
            img->height,
            BIPLANES,
            BIBITCOUNT,
            BICOMPRESSION,
            img->width * img->height * 3,
            BIX,
            BIY,
            BICLRSUSED,
            BICLRIMPORTANT
    };
}

struct image create_image(uint64_t width,uint64_t height){
    return (struct image){width,height,malloc(sizeof(struct pixel)*width*height)};
}

static uint8_t count_paddding(uint32_t width) { return (4 - (width * 3) % 4) % 4; }

static bool signature_is_valid(struct bmp_header bmp) { return bmp.bfType == 0x4d42; }

static bool bits_is_valid(struct bmp_header bmp) { return bmp.biBitCount == 24 && bmp.biCompression == 0; }

static bool header_is_valid(struct bmp_header bmp) { return bmp.bfileSize == bmp.biSizeImage + bmp.bOffBits; }



enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header bmp;
    fread(&bmp, sizeof(struct bmp_header), 1, in);
    if(ferror(in))return READ_ERROR;
    if (!signature_is_valid(bmp)) return READ_INVALID_SIGNATURE;
    if (!bits_is_valid(bmp)) return READ_INVALID_BITS;
    if (!header_is_valid(bmp))return READ_INVALID_HEADER;
    *img= create_image(bmp.biWidth,bmp.biHeight);
    const uint8_t padding = count_paddding(bmp.biWidth);
    uint8_t buf[3];
    for (size_t j = 0; j < bmp.biHeight; ++j) {
        fread(img->data + j * bmp.biWidth, sizeof(struct pixel)*bmp.biWidth, 1, in);
        fread(buf, padding, 1, in);
        if(ferror(in))return READ_ERROR;
    }
    return READ_OK;
}


enum write_status to_bmp(FILE *out, struct image const *img) {
    const struct bmp_header bmp = generate_bmp_header(img);
    const uint8_t padding = count_paddding(bmp.biWidth);
    const uint8_t buf[3] = {0, 0, 0};
    if( fwrite(&bmp, sizeof(struct bmp_header), 1, out)!=1)return WRITE_ERROR;
    for (size_t j = 0; j < bmp.biHeight; ++j) {
        if(fwrite(img->data + j * bmp.biWidth, sizeof(struct pixel)*bmp.biWidth, 1, out)!=1)return WRITE_ERROR;
            
        if(fwrite(buf, padding, 1, out)!=1)return WRITE_ERROR;
    }
    return WRITE_OK;
}
