#include "image.h"
#include <malloc.h>

uint64_t addr_of_pixel(uint32_t x, uint32_t y, uint64_t height){
    return (uint64_t){height * (x + 1) - y};
}

struct image rotate(struct image const source) {
    struct image res = create_image(source.height,source.width);
    uint64_t img_size=res.width*res.height;
    for (size_t j = 0; j < res.height; ++j) {
        for (size_t i = 0; i < res.width; ++i) {
            res.data[i + j * res.width] = source.data[img_size - addr_of_pixel(i,j,res.height)];
        }
    }
    return res;
}

